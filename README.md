=== PROCEDURAL TERRAIN GENERATION ==

Important to note:

	- This tool does not require any dependancies, but it is important to note that it was created in Unity version 2021.3.4f1 using the Universal Render Pipeline (URP).

	- If you are not using URP, simply update the two materials within this package to which ever render pipeline you are using.

Use:

	- This tool is designed to use within prototype projects needing infinitely and/or procedurally generated terrain.

	- For ease of use, the 4 game objects in the "Example Scene" (excluding the camera and directional light), can simply be copied and pasted into a new scene, ready to be used.

	- As demonstrated in the "Example Scene", for this tool to work, the scene requires 4 game objects (3 empties and 1 plane).

		- One empty should have the "Map Generator", "Map Display" and "Endless Terrain" scripts attatched to it.

		- Another empty will act as a temporary display of what the procedural world would look like and should have the "Hide On Play" script attatched to it.

		- The final empty game object will act as the point from which the infinite terrain's LOD system derives its values. This empty game object can also be replaced by a player object.

		- The plane is used to render out the textures used for the generation (noise texture/colour texture) and is not needed to be an active game object.

	- This tool makes use of static scripts, meaning that the procedural generation can only be used in a single scene at a time, beware.

Map Generator Script:

	- Draw Mode:

		- Here, you can determine if the tool visualises the noise map, colour map or mesh in the editor.

	- Normalize Mode:

		- "Global" allows for generated tiles to seamlessly transition between each other.

		- "Local" forces the tiles to generate themselves without considering neighbouring tiles.

	- Use Flat Shading:

		- This option shades the terrain flat by splitting vertices from each other (meaning that the tiles are smaller to keep it from rendering too many vertices).

	- Use Fall Off:

		- This option switches between a singular, infinitely generated terrain and infinite islands.

	- Mesh Height Curve:

		- This curve is used to determine the influence the height has on the generated mesh.

	- Regions:

		- Here, you are able to add different colour regions depending on height.

		- These heights can be manually set (any value between 0 and 1, 0 being the minimum height and 1 being the maximum).

	(The rest of the values in this script is quite self-explanatory and is used to change the general look of the procedural generated terrain).

Map Display Script:

	- Texture Render:

		- This is where the previously mentioned plane is slotted in to display generated colour or noise maps.

	- Mesh Filter and Mesh Renderer:

		- This is a reference to the empty game object on which to display the height and colour maps of the generated terrain.

Endless Terrain Script:

	- Detail Levels:

		- Here, the distance and level of detail for those distances can be set for the tool's LOD system. (The values in the "Example Scene" is recommended).
	- Viewer:

		- Here, the last empty game object (or the player in most cases) is referenced so that the LOD system can determine its values for the system to work.
	- Map Material:

		- This material is used to display the generated colours onto the generated terrain. This material is provided within this package.